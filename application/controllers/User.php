<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function index() {
		$data['title']='Data User';
		$this->load->view('templates/navbar',$data);
		$this->load->view('templates/sidebar',$data); //sidebar and setting menu color
		$this->load->view('user/index',$data);
	}

	public function detail() {
		$data['title']='Detail User';
		$this->load->view('user/index',$data);
	}

	public function edit() {
		$data['title']='Edit User';
		$this->load->view('user/index',$data);
	}

}
