<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function index() {
		$data['title']="Masuk";
		$this->load->view('auth/login',$data);
	}
	public function register_admin() {
		$this->load->model('Auth_models');

		$this->form_validation->set_rules('nm_ktp','Nama','required');
		$this->form_validation->set_rules('no_telp','No HP','required|is_unique[admin.adm_hp]',['is_unique'=>'No. HP Telah Terdaftar!']);
		$this->form_validation->set_rules('tgl_lahir','Tanggal Lahir','required');
		$this->form_validation->set_rules('kelamin','Jenis Kelamin','required');
		$this->form_validation->set_rules('provinsi','Provinsi','required');
		$this->form_validation->set_rules('kabupaten','Kabupaten','required');
		$this->form_validation->set_rules('kecamatan','Kecamatan','required');
		$this->form_validation->set_rules('kelurahan','Kelurahan','required');
		$this->form_validation->set_rules('alamat','Alamat Lengkap','required');
		$this->form_validation->set_rules('password1','Password','required');
		$this->form_validation->set_rules('email','Email','required|is_unique[admin.adm_email]',['is_unique'=>'Email Telah Terdaftar!']);
		$this->form_validation->set_rules('username','Username','required|is_unique[admin.adm_username]',['is_unique'=>'Username Telah Terdaftar!']);

		if( $this->form_validation->run() == FALSE ) {
			$data['title']="Daftar";
			$this->load->view('auth/register_admin',$data);
		} else {
			$this->Auth_models->m_register_admin();
		}
	}

}
