<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function index() {
		$data['title']="Profile";
		$this->load->view('templates/navbar',$data);
		$this->load->view('templates/sidebar',$data);
		$this->load->view('profile/index',$data);
	}	
	public function Edit() {
		$data['title']="Edit Profile";
		$this->load->view('profile/edit',$data);
	}
}
