<?php

class Auth_models extends CI_model {

	public function m_register_admin() {
		$data = [
			"adm_nama" 	=> $this->input->post('nm_ktp'),
			"adm_hp" 	=> $this->input->post('no_telp'),//
			"adm_tgl_lahir" => $this->input->post('tgl_lahir'),
			"adm_jk" 	=> $this->input->post('kelamin'),
			"adm_prov" 	=> $this->input->post('provinsi'),
			"adm_kodya" => $this->input->post('kabupaten'),
			"adm_kec" 	=> $this->input->post('kecamatan'),
			"adm_kel" 	=> $this->input->post('kelurahan'),
			"adm_alamat" => $this->input->post('alamat'),
			"adm_email" => $this->input->post('email'),
			"adm_foto" => $_FILES['foto']['name'],
			"adm_username" 	=> $this->input->post('username'),
			"adm_pwd" => md5($this->input->post('password1')),
			"adm_tgl_buat" => time(),
			"adm_jenis" => "1"
		];
		

		$foto = $_FILES['foto']['name'];//image = name, bukan field
			if ($foto) {
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size'] = '4000';
				$config['upload_path'] = './assets/foto/users/';

				$this->load->library('upload',$config);

				if($this->upload->do_upload('foto')) {
					// data tersimpan
				} else {
					echo $this->upload->display_errors();
				}
			}

		$this->db->insert('admin',$data);
		$this->session->set_flashdata('message','<div class="alert alert-success" role="alert">Akun Berhasil Ditambahkan</div>');
					redirect('auth');
		redirect('auth');
	}

}
?>