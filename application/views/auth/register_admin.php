<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sign Up | Bootstrap Based Admin Template - Material Design</title>
    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?= base_url('assets/');?>plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?= base_url('assets/');?>plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?= base_url('assets/');?>plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?= base_url('assets/');?>css/style.css" rel="stylesheet">

</head>

<body class="signup-page">
    <div class="signup-box">
        <div class="logo">
            <a href="javascript:void(0);">Market Place <b>ADMIN</b></a>
            <!-- <small>Admin BootStrap Based - Material Design</small> -->
        </div>
        <div class="card">
            <div class="body">
                <form id="sign_up" action="<?=base_url('Auth/register_admin');?>" method="POST" enctype="multipart/form-data">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">chrome_reader_mode</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="nm_ktp" placeholder="Nama KTP" required autofocus value="<?= set_value('nm_ktp'); ?>">
                        </div>
                        <?= form_error('nm_ktp','<small class="text-danger pl-3">','</small>'); ?>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">phone_iphone</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="no_telp" placeholder="No. HP" required autofocus value="<?= set_value('no_telp'); ?>">
                        </div>
                        <?= form_error('no_telp','<small class="text-danger pl-3">','</small>'); ?>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">date_range</i>
                        </span>
                        <div class="form-line">
                            <input type="date" class="form-control" name="tgl_lahir" title="Tanggal Lahir" required value="<?= set_value('tgl_lahir'); ?>">
                        </div>
                        <?= form_error('tgl_lahir','<small class="text-danger pl-3">','</small>'); ?>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">people</i>
                        </span>
                        <div class="form-line">
                            <select class="form-control show-tick" name="kelamin">
                                <option>Jenis Kelamin</option>
                                <option value="L">Laki-laki</option>
                                <option value="P">Perempuan</option>
                            </select>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">museum</i>
                        </span>
                        <div class="form-line">
                            <select class="form-control show-tick" name="provinsi">
                                <option>Provinsi</option>
                                <option>123</option>
                                <option>looping here</option>
                            </select>
                        </div>
                    </div>                    
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">local_post_office</i>
                        </span>
                        <div class="form-line">
                            <select class="form-control show-tick" name="kabupaten">
                                <option>Kota/Kab</option>
                                <option>123</option>
                                <option>looping here</option>
                            </select>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">local_post_office</i>
                        </span>
                        <div class="form-line">
                            <select class="form-control show-tick" name="kecamatan">
                                <option>Kecamatan</option>
                                <option>123</option>
                                <option>looping here</option>
                            </select>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">local_post_office</i>
                        </span>
                        <div class="form-line">
                            <select class="form-control show-tick" name="kelurahan">
                                <option>Kelurahan</option>
                                <option>123</option>
                                <option>looping here</option>
                            </select>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">location_on</i>
                        </span>
                        <div class="form-line">
                             <textarea name="alamat" rows="2" class="form-control no-resize" placeholder="Alamat lengkap"><?= set_value('alamat'); ?></textarea>
                        </div>
                        <?= form_error('alamat','<small class="text-danger pl-3">','</small>'); ?>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line">
                            <input type="email" class="form-control" name="email" placeholder="Email Address" required value="<?= set_value('email'); ?>">
                        </div>
                        <?= form_error('email','<small class="text-danger pl-3">','</small>'); ?>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="username" minlength="6" placeholder="Username" required value="<?= set_value('username'); ?>">
                        </div>
                        <?= form_error('username','<small class="text-danger pl-3">','</small>'); ?>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password1" minlength="6" placeholder="Password" required value="<?= set_value('password1'); ?>">
                        </div>
                        <?= form_error('password1','<small class="text-danger pl-3">','</small>'); ?>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password2" minlength="6" placeholder="Konfirmasi Password" required value="<?= set_value('password2'); ?>">
                        </div>
                        <?= form_error('password1','<small class="text-danger pl-3">','</small>'); ?>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">add_a_photo</i>
                        </span>
                        <div class="form-line">
                            <input type="file" class="form-control" name="foto" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="terms" id="terms" class="filled-in chk-col-pink" required="true">
                        <label for="terms">Saya telah membaca dan setuju dengan <a href="javascript:void(0);">Ketentuan Penggunaan</a>.</label>
                    </div>

                    <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">Daftar</button>

                    <div class="m-t-25 m-b--5 align-center">
                        <a href="<?= base_url('auth');?>">Login</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="<?= base_url('assets/');?>plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?= base_url('assets/');?>plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?= base_url('assets/');?>plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="<?= base_url('assets/');?>plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="<?= base_url('assets/');?>js/admin.js"></script>
    <script src="<?= base_url('assets/');?>js/pages/examples/sign-up.js"></script>
</body>

</html>