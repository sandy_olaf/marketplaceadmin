<?php
class Page extends CI_Controller{
  function __construct(){
    parent::__construct();
    if($this->session->userdata('logged_in') !== TRUE){
      redirect('auth/login');
    }
  }

  function index(){
    //Allowing akses to admin only
      if($this->session->userdata('jenis')==='1'){
          $this->load->view('templates/navbar');
          $this->load->view('templates/sidebar');
          $this->load->view('dashboard');
      }else{
          echo "Admin1";
      }

  }

  function user01(){
    //Allowing akses to staff only
    if($this->session->userdata('jenis')==='2'){
      $this->load->view('templates/navbar');
      $this->load->view('templates/sidebar');
      $this->load->view('dashboard');
    }else{
        echo "Admin2";
    }
  }

  function user02(){
    //Allowing akses to author only
    if($this->session->userdata('jenis')==='3'){
      $this->load->view('templates/navbar');
      $this->load->view('templates/sidebar');
      $this->load->view('dashboard');
    }else{
        echo "Admin3";
    }
  }

}
